export type User = {
  id: string;
  username: string;
  wins: number;
  losses: number;
  highscore: number;
  games: number;
};
